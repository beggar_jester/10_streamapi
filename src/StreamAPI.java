import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAPI {
    public static void main(String[] args) {
        System.out.println("Enter a string to print top (max 10) the most common words in it:");
        String inputString = new Scanner(new InputStreamReader(System.in, StandardCharsets.UTF_8)).nextLine();
        System.out.println("Top the most common words:");
        topCommonWords(inputString);
    }

    public static void topCommonWords(String originalString) {
        Arrays.stream(originalString.split(" "))
                .flatMap(text -> Stream.of(text.split("[\\p{Punct}\\s]+")))
                .collect(Collectors.groupingBy(String::toLowerCase, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((word1, word2) ->
                {
                    if (word1.getValue().equals(word2.getValue())) {
                        return word1.getKey().compareTo(word2.getKey());
                    } else {
                        return word2.getValue().compareTo(word1.getValue());
                    }
                })
                .limit(10)
                .forEach(e -> System.out.println(e.getKey()));
    }
}
